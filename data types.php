<?php

//Bolean example started from here

  $decision =true;

  if($decision) {
      echo "The decision is true";
  }

  $decision =false;
  if($decision) {
      echo "The decision is false";
  }
echo"<br>";
//Bolean example ended here


//Integer example started from here

$value =100;
echo $value;
   echo "<br>";

//Integer example ended here





//Float/Double example started from here

$value = 64.56;
echo $value;
echo"<br>";

//Float/Double example ended from here



//String example started here
$var = 100;
$string1 = 'This is single quoted string $var<br>';
$string2 = "This is double quoted string $var<br>";
echo $string1.$string2;

$heredocString =<<<BITM
    this is heredoc example line1  var$ <br>
    this is heredoc example line2  var$ <br>
    this is heredoc example line3  var$ <br>
    this is heredoc example line4  var$ <br>

BITM;


$nowdocString =<<<'BITM'
      this is nowdoc example line1  var$ <br>
      this is nowdoc example line2  var$ <br>
      this is nowdoc example line3  var$ <br>
      this is nowdoc example line4  var$ <br>



BITM;

echo $heredocString . "<br><br>" . $nowdocString;

//String example ended here



$arr = array(1,2,3,4,5,6,7,8,9,10);
print_r($arr);
echo"<br>";

$arr = array("BMW","TOYOTA","nissan","Form","MARUTI","ford");
print_r($arr);
echo"<br>";

$ageArray = array("Arif"=>30,"Riaz"=>40,"Rahim"=>35);
print_r($ageArray);
echo"<br>";

echo"The age of Riaz is" . $ageArray["Riaz"];

echo"<br>";


